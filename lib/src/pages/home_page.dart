import 'package:flutter/material.dart';
import 'package:componentes/src/providers/menu_provider.dart';
import 'package:componentes/src/utils/icon_string_util.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes'),
      ),
      body: _lista(),
    );
  }

  Widget _lista() {
    //? No se Usa esta manera ya que puede demorar mucho en producción y causar una mala experiencia
    /*menuProvider.cargarData().then((opciones) {
      print('_lista');
      print(opciones);
    });
    */

    return FutureBuilder(
      future: menuProvider.cargarData(),
      initialData: [],
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        return ListView(
          children: _listaItems(snapshot.data!, context),
        );
      },
    );

    //return
  }

  List<Widget> _listaItems(List<dynamic> data, BuildContext context) {
    final List<Widget> opciones = [];

    data.forEach((opt) {
      final widgetTemp = ListTile(
          title: Text(opt['texto']),
          leading: getIcon(opt['icon']),
          trailing:
              const Icon(Icons.keyboard_arrow_right, color: Colors.blueAccent),
          onTap: () {
            Navigator.pushNamed(context, opt['ruta']);

            //final route = MaterialPageRoute(builder: (context) => AlertPage());
            //Navigator.push(context, route);
          });
      opciones
        ..add(widgetTemp)
        ..add(Divider());
    });

    return opciones;
  }
}
