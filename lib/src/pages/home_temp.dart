import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {
  HomePageTemp({super.key});

  final opciones = ['Uno', 'Dos', 'Tres', 'Cuatro', 'Cinco'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Componentes Temp'),
        ),
        body: ListView(
          //children: _crearItems(),
          children: _crearItemsCorta(),
        ));
  }

  List<Widget> _crearItems() {
    List<Widget> lista = List<Widget>.from([]);

    for (var opt in opciones) {
      final tempWidget = ListTile(title: Text(opt));

      lista
        ..add(tempWidget)
        ..add(const Divider(
          height: 5.0,
          color: Color.fromRGBO(10, 10, 10, 1),
        ));
    }

    return lista;
  }

  List<Widget> _crearItemsCorta() {
    return opciones.map((item) {
      return Column(
        children: [
          ListTile(
            title: Text(item),
            subtitle: const Text('Cualquier cosa'),
            leading: Icon(Icons.ac_unit_sharp),
            trailing: Icon(Icons.arrow_back_ios_new_outlined),
            onTap: () {},
          ),
          const Divider(
            color: Color.fromRGBO(10, 10, 10, 1),
          )
        ],
      );
    }).toList();
  }
}
